# Tower_Defense_Game

Tower Defense Game with Unity game engine.

![Tower Defense Game](TowerDefenseGif.gif)

The game has the following features;
 - 3 different types of enemies:
   - Simple
   - Fast
   - Tough
 - 3 different types of turret:
   - Simple Turret
   - Laser Beamer
   - Missile Launcher
 - Random Map Generator
 - Different Particle Effects
 - Turret Upgrade/Sell System
 - Main Menu / Game Over / Level Success screens
 - 10 Different Level
