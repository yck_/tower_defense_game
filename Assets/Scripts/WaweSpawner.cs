using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class WaweSpawner : MonoBehaviour
{   
    public static int EnemiesAlive = 0;

    public Wave [] waves;

    public Transform spawnPoint=null;

    public float timeBetweenWawes  = 5f;
    private float countdown = 2f;

    private int waveNumber=0;

    public SceneFader sceneFader;

    public GameObject completeLevelUI;
    
    void Update(){
        
        if(EnemiesAlive > 0){
            return;
        }

        if(countdown <= 0){
            StartCoroutine(spawnWawe());
            countdown = timeBetweenWawes;
            return;
        }
        
        /*countdown by 1 in every second*/
        countdown -= Time.deltaTime;
    }   

    IEnumerator spawnWawe(){
        PlayerStats.rounds++;
        
        if(waveNumber < waves.Length){
            Wave wave = waves[waveNumber]; 

            for (int i = 0; i < wave.count; i++){
                spawnEnemy(wave.enemy);
                yield return new WaitForSeconds(1f / wave.rate);
            }
            waveNumber ++;
        }else{
            Debug.Log("LEVEL WON!");
            if(EnemiesAlive == 0){
                GameManager.gameEnded = true;
                completeLevelUI.SetActive(true);
            }
        }
    }

    // private void endLevel(){
    //     Debug.Log("LEVEL WON!");
    //     Debug.Log(SceneManager.GetActiveScene().buildIndex.ToString());
    //     int nextScene = SceneManager.GetActiveScene().buildIndex+1%11;
    //     if(nextScene==12){
    //         sceneFader.FadeTo("MainMenu");
    //     }
    //     sceneFader.FadeTo("Level0"+nextScene.ToString());
    //     this.enabled = false;
    // }

    void spawnEnemy(GameObject enemy){
        Instantiate(enemy,spawnPoint.position,spawnPoint.rotation);
        EnemiesAlive++;
    }





}
