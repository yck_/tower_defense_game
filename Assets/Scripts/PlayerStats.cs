using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{   
    public static int Money;
    public int startMoney = 300;

    public static int Lives;
    public int startLives = 20;

    public static int rounds;

    void Start() {
        /*We set start money this way because when new level loaded we want user money to reset.*/
        Money = startMoney;
        Lives = startLives;
        rounds = 0;
    }



}
