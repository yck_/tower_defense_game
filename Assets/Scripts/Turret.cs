using UnityEngine;

public class Turret : MonoBehaviour
{
    private Transform target;
    private Enemy targetEnemy;
    
    [Header("General")]
    public float range = 15f;

    [Header("Use Bullets (default)")]
    public float fireRate = 1f;
    public float fireCountDown = 0f;
    public GameObject bulletPrefab;

    [Header("Use Laser")]
    public bool useLaser = false;
    public int damageOverTime = 30;
    public float slowAmount = .9f;
    
    public LineRenderer lineRenderer;
    public ParticleSystem impactEffect;
    public Light impactLight;

    [Header("Unity Setup Fields")]
    public string enemyTag = "Enemy";

    public Transform piwotPoint;
    public float turnSpeed = 10f;
    public Transform firePoint;



    // Start is called before the first frame update
    void Start(){
        InvokeRepeating("UpdateTarget",0f,0.5f);
        piwotPoint.rotation=Quaternion.Euler(0f,0f,0f);
    }

    // This method does not work on update. 
    // Instead it works on new threead that checks only few times in a second.
    void UpdateTarget(){
        
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (GameObject enemy in enemies){
            float distanceToEnemy = Vector3.Distance(transform.position,enemy.transform.position);

            if(distanceToEnemy < shortestDistance){
                shortestDistance=distanceToEnemy;
                nearestEnemy=enemy;
            }
        }
        if(nearestEnemy!=null && shortestDistance <= range){
            target = nearestEnemy.transform;
            targetEnemy = nearestEnemy.GetComponent<Enemy>();
        }else{
            target=null;
        }

    }


    void Update(){
        if(target==null){
            
            if(useLaser){
                if(lineRenderer.enabled){
                    lineRenderer.enabled = false;
                    impactEffect.Stop();
                    impactLight.enabled=false;
                }
            }

            return;
        }

        rotateToEnemy();

        if(useLaser){
            laserShoot();
        }else{
            bulletShoot();
        }

    }

    void laserShoot(){

        targetEnemy.TakeDamage(damageOverTime*Time.deltaTime);
        targetEnemy.Slow(slowAmount);
        if(!lineRenderer.enabled){
            lineRenderer.enabled = true;
            impactEffect.Play();
            impactLight.enabled=true;
        }

        lineRenderer.SetPosition(0,firePoint.position);
        lineRenderer.SetPosition(1,target.position);

        Vector3 dir = firePoint.position - target.position;
        
        impactEffect.transform.position = target.position + dir.normalized;

        impactEffect.transform.rotation =  Quaternion.LookRotation(dir);
    }

    void bulletShoot(){
        if(fireCountDown <= 0){
            shoot();
            fireCountDown = 1f / fireRate;
        }

        fireCountDown -= Time.deltaTime;
    }

    void shoot(){
		GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
		Bullet bullet = bulletGO.GetComponent<Bullet>();

        if(bullet != null){
            bullet.seek(target);
        }
    }


    void rotateToEnemy(){
        Vector3 direction = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        Vector3 rotation = Quaternion.Lerp(piwotPoint.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        piwotPoint.rotation = Quaternion.Euler(0f,rotation.y,0f);
    }

    void OnDrawGizmosSelected() {
        Gizmos.color=Color.red;
        //draw sphere which only wire parts are showing
        Gizmos.DrawWireSphere(transform.position,range);
    }
}
