using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CompleteLevel : MonoBehaviour
{
    public SceneFader sceneFader;
    public string menuSceneName = "MainMenu";

    public void Continue(){
        endLevel();
    }

    void Menu(){
        sceneFader.FadeTo(menuSceneName);
    }

    private void endLevel(){
        int nextScene = SceneManager.GetActiveScene().buildIndex%11;
        if(nextScene==0){
            sceneFader.FadeTo("MainMenu");
        }else{
            Debug.Log("GOING TO LEVEL: "+ "Level0"+nextScene.ToString());
            sceneFader.FadeTo("Level0"+nextScene.ToString());
            this.enabled = false;
        }
    }
}
