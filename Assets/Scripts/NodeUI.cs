using UnityEngine;
using UnityEngine.UI;

public class NodeUI : MonoBehaviour
{

    public GameObject ui;
    private TileManager target;
    public Text upgradeCost;
    public Text sellAmount;
    public Button upgradeButton;


    public void SetTarget(TileManager node){
        this.target = node;
        transform.position = node.getBuildPosition();

        if(!target.isUpgraded){
            upgradeCost.text = "$"+target.turretBlueprint.upgradeCost;
            upgradeButton.interactable = true;
        }else{
            upgradeCost.text = "DONE";
            upgradeButton.interactable = false;
        }

        sellAmount.text =  "$"+target.turretBlueprint.getSellAmount();



        ui.SetActive(true);
    }

    public void hide(){
        ui.SetActive(false);
    }

    public void Upgrade(){
        target.UpgradeTurret();
        BuildManager.getInstance().deselectNode();
    }

    public void Sell(){
        target.sellTurret();
        BuildManager.getInstance().deselectNode();
    }



}
