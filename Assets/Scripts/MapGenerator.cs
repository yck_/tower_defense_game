using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapGenerator : MonoBehaviour
{      
    public const int LEFT = 0;
    public const int RIGHT = 1;

    public GameObject tile;
    [SerializeField] int width;
    [SerializeField] int height;

    private List<GameObject> allTiles;
    public static List<GameObject> pathTiles;

    public Canvas topCanvas;

    void Start() {
        allTiles = new List<GameObject>();
        pathTiles = new List<GameObject>();
        generateMap();
        generatePath();
        setPathTilesOnTileManager();
        setGameMaster();
        setTopCanvasPosition();
    }

    void setTopCanvasPosition(){
        float w = topCanvas.GetComponent<RectTransform>().rect.width;
        float h = topCanvas.GetComponent<RectTransform>().rect.height;
        Vector3 newPosition = new Vector3(width*2,0,height*4+4);
        topCanvas.transform.position=newPosition;
    }

    void setGameMaster(){
        GameObject gamemaster = GameObject.FindWithTag("gamemaster");
        gamemaster.GetComponent<WaweSpawner>().spawnPoint = pathTiles[0].transform;
    }

    void generateMap(){
        for(int i=0;i<width;i++){
            for(int j=0;j<width;j++){
                GameObject tileAdd = Instantiate(tile);
                tileAdd.transform.parent=this.transform;
                tileAdd.transform.position = new Vector3(4*j,0,4*i);
                allTiles.Add(tileAdd);
            }
        }
    }

    /*Generates game path*/
    void generatePath(){

        /*Init start tile*/
        pathTiles.Add(getRandomStartAtTop());
        GameObject currentTile = pathTiles[0];
        GameObject nextTile;
        /*Until reacing the bottom of the tiles*/
        while(currentTile.transform.position.z != 0){
            nextTile = addRandomTile(currentTile);
            currentTile = (nextTile!=null && pathTiles[pathTiles.Count-1].Equals(nextTile)) ? nextTile : currentTile;
        }

        completePathWithRandomEndPoint();
        changePathTilesColor();
    }

    GameObject getRandomStartAtTop(){
        int index = Random.Range(0, width);
        return allTiles[width*(height-1) + index];
    }

    GameObject getRandomEndAtBottom(){
        int index = Random.Range(0, width);
        return allTiles[index];
    }

    GameObject addRandomTile(GameObject currentTile){
        /*generate random path*/
        int randIndex=Random.Range(0,3);
        GameObject nextTile;
        if(randIndex==LEFT){
            nextTile=addLeftToPathTiles(currentTile);
        }
        else if(randIndex==RIGHT){
            nextTile=addRightToPathTiles(currentTile);
        }
        else{
            nextTile=addBottomToPathTiles(currentTile);
        }
        return nextTile;
    }

    GameObject addLeftToPathTiles(GameObject currentTile){
        GameObject nextTile;
        nextTile=getLeftTile(currentTile);
        if(nextTile!=null && !pathTiles.Contains(nextTile)){
            pathTiles.Add(nextTile);
        }
        return nextTile;
    }

    GameObject addRightToPathTiles(GameObject currentTile){
        GameObject nextTile;
        nextTile=getRightTile(currentTile);
        if(nextTile!=null && !pathTiles.Contains(nextTile)){
            pathTiles.Add(nextTile);
        }
        return nextTile;
    }

    GameObject addBottomToPathTiles(GameObject currentTile){
        GameObject nextTile;
        nextTile=getBottomTile(currentTile);
        if(nextTile!=null && !pathTiles.Contains(nextTile)){
            pathTiles.Add(nextTile);
        }
        return nextTile;
    }

    GameObject getLeftTile(GameObject currentTile){
        GameObject leftTile = null;
        int currentTileIndex = allTiles.IndexOf(currentTile);
        if(currentTile.transform.position.x != 0){
            leftTile = allTiles[currentTileIndex-1];
        }
        return leftTile;
    }

    GameObject getRightTile(GameObject currentTile){
        GameObject rightTile = null;
        int currentTileIndex = allTiles.IndexOf(currentTile);
        if(currentTile.transform.position.x != width*4-4){
            rightTile = allTiles[currentTileIndex+1];
        }
        return rightTile;
    }

    GameObject getBottomTile(GameObject currentTile){
        GameObject bottomTile = null;
        int currentTileIndex = allTiles.IndexOf(currentTile);
        if(currentTile.transform.position.z != 0){
            bottomTile = allTiles[currentTileIndex-width];
        }
        return bottomTile;
    }


    void completePathWithRandomEndPoint(){
        GameObject endTile = getRandomEndAtBottom();

        if(pathTiles.Contains(endTile)) return;
    }

    void completePathUntilReachLeft(GameObject targetTile){
        GameObject currentTile = pathTiles[pathTiles.Count-1];
        while(!currentTile.Equals(targetTile)){
            currentTile = addLeftToPathTiles(currentTile);
        }
    }

    void completePathUntilReachRight(GameObject targetTile){
        GameObject currentTile = pathTiles[pathTiles.Count-1];
        while(!currentTile.Equals(targetTile)){
            currentTile = addRightToPathTiles(currentTile);
        }
    }

    void changePathTilesColor(){
        for(int i=0;i<pathTiles.Count;i++){
            pathTiles[i].GetComponent<Renderer>().material.color = Color.gray;
        }

        pathTiles[0].GetComponent<Renderer>().material.color = Color.blue;
        pathTiles[0].GetComponent<Transform>().localScale = new Vector3(4,4,4);
        pathTiles[0].GetComponent<Transform>().position += new Vector3(0,2,0);

        pathTiles[pathTiles.Count-1].GetComponent<Renderer>().material.color = Color.green;
        pathTiles[pathTiles.Count-1].GetComponent<Transform>().localScale = new Vector3(4,4,4);
        pathTiles[pathTiles.Count-1].GetComponent<Transform>().position += new Vector3(0,2,0);

    }

    void setPathTilesOnTileManager(){
        for(int i=0;i<pathTiles.Count;i++){
            pathTiles[i].GetComponent<TileManager>().isPath=true;
        }
    }

    
}
