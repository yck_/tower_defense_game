using UnityEngine.EventSystems;
using UnityEngine;

public class TileManager : MonoBehaviour{


    public Color hoverColor;
    public Color notEnoughMonetColor;
    public Color defaultColor;
    private Renderer rend;
    public bool isPath = false;

    public Vector3 offset;

    BuildManager buildManager;

    /*represents turret built on tile, if no turret variable is null*/
    [HideInInspector]
    public GameObject turret;

    [HideInInspector]
    public TurretBlueprint turretBlueprint;

    [HideInInspector]
    public bool isUpgraded = false;



    void Start(){
        buildManager = BuildManager.getInstance();
        rend=GetComponent<Renderer>();
    }

    public Vector3 getBuildPosition(){
        return transform.position + offset;
    }

    void OnMouseDown() {

        if(EventSystem.current.IsPointerOverGameObject()){
            return;
        }

        if(turret != null || isPath){
            buildManager.selectNode(this);
            return;
        }

        if(!buildManager.canBuild){
            return;
        }

        //buildManager.buildTurretOn(this);
        buildTurret(buildManager.getTurretToBuild());
    }

    void buildTurret (TurretBlueprint blueprint){

        if(PlayerStats.Money<blueprint.cost){
            Debug.Log("Not enough money to build that!");
            return;
        }

        PlayerStats.Money -= blueprint.cost;
        GameObject _turret = (GameObject)Instantiate(blueprint.prefab,getBuildPosition(),Quaternion.identity);
        turret = _turret;

        turretBlueprint = blueprint;

        GameObject effect = (GameObject)Instantiate(buildManager.buildEffect,getBuildPosition(),Quaternion.identity);
        Destroy(effect,5f);

        Debug.Log("Turret build!");
    }

    public void UpgradeTurret(){
        
        if(PlayerStats.Money<turretBlueprint.upgradeCost){
            Debug.Log("Not enough money to upgrade that!");
            return;
        }

        PlayerStats.Money -= turretBlueprint.upgradeCost;

        Destroy(turret);

        GameObject _turret = (GameObject)Instantiate(turretBlueprint.upgradedPrefab,getBuildPosition(),Quaternion.identity);
        turret = _turret;

        GameObject effect = (GameObject)Instantiate(buildManager.buildEffect,getBuildPosition(),Quaternion.identity);
        Destroy(effect,5f);

        isUpgraded = true;

        Debug.Log("Turret build!");
    }

    public void sellTurret(){
        PlayerStats.Money += turretBlueprint.getSellAmount();
        
        GameObject effect = (GameObject)Instantiate(buildManager.sellEffect,getBuildPosition(),Quaternion.identity);
        Destroy(effect,5f);

        Destroy(turret);
        turretBlueprint=null;
        isUpgraded = false;
    }

    void OnMouseEnter() {

        if(EventSystem.current.IsPointerOverGameObject()){
            return;
        }

        if(!buildManager.canBuild){
            return;
        }

        if(!isPath){ 

            if(buildManager.hasMoney){
                rend.material.color = hoverColor;
            }else{
                rend.material.color = notEnoughMonetColor;
            }
        }
    }

    void OnMouseExit(){
        if(!isPath){
            rend.material.color = defaultColor;
        }
    }



}
