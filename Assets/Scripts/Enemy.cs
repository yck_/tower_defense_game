using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{   
    EnemyMovement enemyMovement;

    public float startHealth = 100;

    private  float health;

    public int worth = 50;

    private GameObject nextTile;

    private GameObject endTile;

    public GameObject deathEffect;

    private bool isDead = false;

    [Header("Unity Stuff")]
    public Image healthBar;

    private void Start() {
        enemyMovement=this.GetComponent<EnemyMovement>();
        health = startHealth;
    }

    public void Slow(float pct){
        enemyMovement.Slow(pct);
    }

    public void TakeDamage(float amount){
        health -= amount;

        healthBar.fillAmount = health / startHealth;

        if(health<=0 && !isDead){
            Die();
        }
    }

    void Die(){

        isDead = true;
        PlayerStats.Money += worth;

        GameObject effect = (GameObject)Instantiate(deathEffect,transform.position,Quaternion.identity);
        Destroy(effect,5f);

        WaweSpawner.EnemiesAlive--;

        Destroy(gameObject);
    }


    public void destroyEnemy(){
        WaweSpawner.EnemiesAlive--;
        PlayerStats.Lives--;
        Destroy(gameObject);
    }

}
