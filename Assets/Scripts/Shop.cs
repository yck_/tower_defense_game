using UnityEngine;

public class Shop : MonoBehaviour
{

    public TurretBlueprint standardTurret;
    public TurretBlueprint missileLauncher;
    public TurretBlueprint laserBeamer;

    BuildManager buildManager;

    private void Start() {
        buildManager = BuildManager.getInstance();
    }

    public void selectStandardTurret(){
        Debug.Log("Standart turret selected.");
        buildManager.selectTurretToBuild(standardTurret);
    }

    public void selectMissileLaucher(){
        Debug.Log("Missile Launcher selected.");
        buildManager.selectTurretToBuild(missileLauncher);
    }

    public void selectLaserBeamer(){
        Debug.Log("Laser Beamer selected.");
        buildManager.selectTurretToBuild(laserBeamer);
    }


}
