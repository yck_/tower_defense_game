using UnityEngine;

public class BuildManager : MonoBehaviour
{

    /*Singleton Pattern parameter*/
    public static BuildManager Instance;
    private TurretBlueprint turretToBuild;
    private TileManager selectedNode;
    public GameObject sellEffect;

    public GameObject buildEffect;

    public NodeUI nodeUI;

    private void Awake() {
        
        /*Sıngleton pattern*/
        if(Instance!=null && Instance != this){
            Debug.LogError("More than one BuildManager in scene!");
            Destroy(this);
            return;
        }else{ 
            Instance = this; 
        } 

    }

    public bool canBuild{ get{ return turretToBuild != null;} }

    public bool hasMoney{ get{ return PlayerStats.Money>=turretToBuild.cost;} }

    public static BuildManager getInstance(){
        return Instance;
    }

    public void selectNode(TileManager node){

        if(selectedNode == node || node.isPath){
            deselectNode();
            return;
        }

        selectedNode = node;
        turretToBuild = null;
        
        nodeUI.SetTarget(node);
    }

    public void deselectNode(){
        selectedNode=null;
        nodeUI.hide();
    }

    public void selectTurretToBuild(TurretBlueprint turret){
        turretToBuild = turret;
        deselectNode();
    }

    public TurretBlueprint getTurretToBuild(){
        return turretToBuild;
    }



}
