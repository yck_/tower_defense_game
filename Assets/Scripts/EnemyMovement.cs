using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{

    public float startSpeed = 5;

    private float movementSpeed;

    private GameObject nextTile;

    private GameObject endTile;

    private Enemy myEnemy;


    void Start()
    {
        movementSpeed = startSpeed;
        myEnemy = GetComponent<Enemy>();
        initializeEnemy();
    }

    private void Update() {
        checkPosition();
        move((nextTile.transform.position + new Vector3(0,2,0)));
    }

    private void initializeEnemy(){
        transform.position = MapGenerator.pathTiles[0].transform.position + new Vector3(0,2,0);
        nextTile = MapGenerator.pathTiles[1];
        endTile = MapGenerator.pathTiles[MapGenerator.pathTiles.Count-1];
    }

    private void move(Vector3 nextTilePosition){
        Vector3 direction = nextTilePosition - transform.position;
        transform.Translate(direction.normalized*movementSpeed*Time.deltaTime,Space.World);

        movementSpeed = startSpeed;
    }

    private void checkPosition(){
        if(nextTile != null){ 
            float delta = (transform.position - (nextTile.transform.position+ new Vector3(0,2,0))).magnitude;

            if(nextTile != endTile){
                if(delta < 0.2f){
                    nextTile = MapGenerator.pathTiles[MapGenerator.pathTiles.IndexOf(nextTile)+1];
                }
            }

            if(nextTile==endTile){
                if(delta < 0.1f){
                   myEnemy.destroyEnemy();
                }
            }
        }
    }

    
    public void Slow(float pct){
        movementSpeed = startSpeed * (1f-pct);
    }

}
